package br.com.itau.customer.controllers;

import br.com.itau.customer.models.Customer;
import br.com.itau.customer.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/customer")

public class CustomerController {


    @Autowired
    CustomerService customerService;

    @PostMapping
    public Customer create(@RequestBody @Valid Customer customer) {
        return customerService.create(customer);
    }

    @GetMapping("/{id}")
    public Customer getById(@PathVariable Long id) {
        Customer customer = customerService.getById(id);
        return customer;
    }
}
